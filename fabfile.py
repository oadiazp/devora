# -*- coding: utf-8 -*-

from __future__ import with_statement
from fabric.api import env, require, run, sudo, cd, prefix
from fabric.contrib.files import exists

#--------------------------------------
#environments
#--------------------------------------


def amazon():
    "Servidor local de prueba"
    env.name = 'amazon'
    env.user = 'ubuntu'
    env.project_name = 'devora'
    env.project_root = '/home/%(user)s/apps/%(project_name)s/src' % env
    env.project_path = '/home/%(user)s/apps/%(project_name)s' % env
    env.hosts = ['zczoft.com']
    env.branch = 'master'
    env.repo = 'git@bitbucket.org:oadiazp/devora.git'
    env.venv = '/home/%(user)s/apps/%(project_name)s/venv' % env
    env.download_path = '/home/%(user)s/apps/%(project_name)s/src/media/download/' % env


def install_ubuntu_dependencies():
    sudo('apt-get -y install nginx postgresql php5-fpm')
    sudo('apt-get -y install php5-cli php5-gd php5-curl')
    sudo('apt-get -y install git php5-pgsql')


def common():
    with cd(env.project_root):
        run('php composer.phar update')
        run('php app/console cache:clear --env=prod')
        sudo("chmod -R 777 app/")
        
        #actualizar los settings de nginx
        sudo("cp -Rf conf/%s/nginx /etc/nginx/sites-available/%s" % (env.name, env.project_name))
        sudo("cp -Rf conf/%s/fastcgi_params %s" % (env.name, env.project_root))
        sudo("ln -sf /etc/nginx/sites-available/%s /etc/nginx/sites-enabled/%s" % (env.project_name, env.project_name))

        #reiniciar el servicio
        sudo("service nginx restart")
       

def clone_repository():
    run('git clone %s %s' % (env.repo, env.project_root))

    with cd(env.project_root):
        run('git checkout %s' % env.branch)

def update():
    require('name')
    require('project_root')

    with cd(env.project_root):
        #darle un pull al servidor
        run('git reset --hard')
        run('git pull origin %s' % env.branch)
        common()

def create_database():
    with cd(env.project_root):
        run('php app/console doctrine:database:create')
        run('php app/console doctrine:schema:create')

def create_virtualenv():
    run("virtualenv %s" % env.venv)

def make_directories():
    run("mkdir %s/log" % env.project_path)
    run("mkdir -p %s" % env.download_path)


def install():
    install_ubuntu_dependencies()
    clone_repository()
    make_directories()
    common()

    create_database()
<?php

namespace AppBundle\Service;
use AppBundle\Entity\Contacto;
use Swift_Message;

class Logic {
    protected $mailer;
    protected  $templating;


    public function __construct($mailer, $templating) {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function process(Contacto $contacto)
    {
        $msg = Swift_Message::newInstance()
                ->setSubject("Nuevo contacto ".date('d-m-Y H:i:s P'))
                ->setFrom("devoradvip@devoradvip.com")
                ->setTo("devoradvip@nauta.cu")
                ->setBody($this->templating->render('AppBundle:mail:contact.txt.twig', array('contacto' => $contacto)));
        $this->mailer->send($msg);
        $contacto->setEnviado(true);
        return $contacto;
    }
}

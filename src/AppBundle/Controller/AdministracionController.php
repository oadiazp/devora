<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\BannerType;
use AppBundle\Entity\Banner;

class AdministracionController extends Controller{
    /**
     * @Route("/administracion/", name="home_admin")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:administracion:home.html.twig', array());
    }
    
    /**
     * @Route("/administracion/banner", name="list_banner")
     */
    public function listBannerAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle\Entity\Banner');
        $results = $repo->getAll();
        return $this->render('AppBundle:administracion:listBanner.html.twig', array('results'=>$results));
    }
    
    /*Obvio...*/
    protected function getItem($repo_name, $id)
    {
        $repo = $this->getDoctrine()->getRepository($repo_name);
        $item = $repo->find($id);
        if($item == null)
        {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }
        return $item;
    }
    
    /**
     * @Route("/administracion/banner_cambiar_estado/{id}", name="cambiar_estado_banner")
     */
    public function cambiarEstadoAction($id)
    {
        $item = $this->getItem('AppBundle\Entity\Banner', $id);
        $next = true;
        if($item->getActivo())
        {
            $next = false;
        }
        $item->setActivo($next);
        $em = $this->getDoctrine()->getManager();
        $em->persist($item);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Se ha modificado el estado del banner');
        return $this->redirect($this->generateUrl('list_banner'));
    }


    /**
     * @Route("/administracion/banner_eliminar/{id}", name="remove_banner")
     */
    public function eliminarBannerAction($id)
    {
        $item = $this->getItem('AppBundle\Entity\Banner', $id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Se ha eliminado el banner');
        return $this->redirect($this->generateUrl('list_banner'));
    }

    /**
     * @Route("/administracion/banner_crear", name="create_banner")
     */
    public function createBannerAction()
    {
        $banner = new Banner();
        $form = $this->createForm(new BannerType(), $banner);
        $request = $this->getRequest();
        if($request->isMethod("POST"))
        {
            $form->handleRequest($request);
            if($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($banner);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Se ha agregado un nuevo banner');
                return $this->redirect($this->generateUrl('list_banner'));
            }
            else
            {
                $this->get('session')->getFlashBag()->add('fail', 'No se pudo agregar el banner');
            }
        }
        return $this->render('AppBundle:administracion:createBanner.html.twig', array('form'=>$form->createView()));
    }
    
    /**
     * @Route("/administracion/contacto_list", name="list_contacto")
     */
    public function listContactoAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle\Entity\Contacto');
        $results = $repo->getAllRecentlyFirst();
        return $this->render('AppBundle:administracion:listContacto.html.twig', array('results'=>$results));;
    }
}

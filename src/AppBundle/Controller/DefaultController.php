<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\ContactoFrontendType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle\Entity\Banner');
        $banners = $repo->getActivos();
        
        $form = $this->createForm(new ContactoFrontendType());
        
        $request = $this->getRequest();
        if($request->isMethod("POST"))
        {
            $form->handleRequest($request);
            if($form->isValid())
            {
                $contacto = $form->getData();
                $contacto->setIp($request->getClientIp());
                $logic = $this->get('app.logic');
                $logic->process($contacto);
                $em = $this->getDoctrine()->getManager();
                $em->persist($contacto);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Gracias por contactarnos, responderemos su mensaje antes de 24 horas');
                return $this->redirect($this->generateUrl('homepage'));
            }
            else
            {
                $this->get('session')->getFlashBag()->add('fail', 'No se ha podido enviar el mensaje.');
            }
        }
        
        return $this->render('AppBundle:default:home.html.twig', array('banners' => $banners, 'form' => $form->createView()));
    }
}

<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Regex;
class ContactoFrontendType extends AbstractType{
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
        $builder->add('nombre', 'text',array('label'=>'Su nombre', 
            'constraints' => array(
                new Regex(array('pattern' => '/[Aa-zZ]/'))
            )));
        $builder->add('correo', 'email', array('label'=>'Su dirección de correo'));
        $builder->add('nota', 'textarea', array('required'=>false));
        //$builder->add('captcha', 'captcha');
    }
    
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver) {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array('data_class'=>'AppBundle\Entity\Contacto'));
    }
    
    public function getName() {
        return 'contacto';
    }
}

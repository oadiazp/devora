<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;

class BannerType extends AbstractType{
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
        $builder->add('nombre', 'text');
        $builder->add('archivo', 'file');
        $builder->add('activo', 'choice', array('choices' => array('true' => 'Si', 'false' => 'No')));
    }
    public function getName() {
        return 'banner_type';
    }
}

<?php
namespace UserBundle\Form;

use FOS\UserBundle\Form\Type\RegistrationFormType;
class UsuarioEditType extends RegistrationFormType{
    public function __construct() {
        parent::__construct('UserBundle\Entity\Usuario');
    }
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
        $builder->add('nombre', 'text');
        parent::buildForm($builder, $options);
    }
    
    public function getName() {
       return 'crear_usuario';
    }
}

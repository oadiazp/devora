<?php

namespace UserBundle\Form;

use FOS\UserBundle\Form\Type\RegistrationFormType;

class UsuarioCreateType extends RegistrationFormType{
    public function __construct() {
        parent::__construct('UserBundle\Entity\Usuario');
    }
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
        $builder->add('nombre', 'text');
        $builder->add('enabled', 'choice', array('true'=>'Si', 'false'=>'No'));
        parent::buildForm($builder, $options);
    }
    
    public function getName() {
       return 'crear_usuario';
    }
}

<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Entity\Usuario;
use UserBundle\Form\UsuarioCreateType;
use UserBundle\Form\UsuarioEditType;

class DefaultController extends Controller
{
    /*Obvio...*/
    protected function getItem($repo_name, $id)
    {
        $repo = $this->getDoctrine()->getRepository($repo_name);
        $item = $repo->find($id);
        if($item == null)
        {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }
        return $item;
    }
    
    public function createAction()
    {
        $user = new Usuario();
        $form = $this->createForm(new UsuarioCreateType(), $user);
        $request = $this->getRequest();
        if($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if($form->isValid())
            {
                $service = $this->get('fos_user.user_manager');
                $service->updateUser($user);
                $this->get('session')->getFlashBag()->add('success', 'Se ha creado un usuario');
                return $this->redirect($this->generateUrl('user_homepage'));
            }
            else
            {
                $this->get('session')->getFlashBag()->add('fail', 'No se pudo crear el usuario');
            }
        }
        return $this->render('UserBundle:default:create.html.twig', array('form'=>$form->createView()));
    }
    
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('UserBundle\Entity\Usuario');
        $results = $repo->getAll();
        return $this->render('UserBundle:default:index.html.twig', array('results'=>$results));
    }
    
    public function removeAction($id)
    {
        $user = $this->getItem('UserBundle\Entity\Usuario', $id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Se elminó el usuario '.$user->getUsername());
        return $this->redirect($this->generateUrl('user_homepage'));
    }

    public function editAction($id)
    {
        $user = $this->getItem('UserBundle\Entity\Usuario', $id);
        $form = $this->createForm(new UsuarioEditType(), $user);
        $request = $this->getRequest();
        if($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            if($form->isValid())
            {
                $service = $this->get('fos_user.user_manager');
                $service->updateUser($user);
                $this->get('session')->getFlashBag()->add('success', 'Se ha guardado la información del usuario');
                return $this->redirect($this->generateUrl('user_homepage'));
            }
        }
        else
        {
                $this->get('session')->getFlashBag()->add('fail', 'No se pudo guardar información del usuario');
        }
        return $this->render('UserBundle:default:edit.html.twig', array('form' => $form->createView()));
    }
}
